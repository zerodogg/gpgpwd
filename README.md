# gpgpwd

gpgpwd is a terminal-based password manager. It stores a list of passwords
in a GnuPG encrypted file, and allows you to easily retrieve, change and add to
that file as needed. It also generates random passwords that you can use,
easily allowing you to have one "master password" (for your gpg key), with
one unique and random password for each website or service you use, ensuring
that your other accounts stay safe even if one password gets leaked.

gpgpwd can also utilize git to allow you to easily synchronize your
passwords between different machines.

## Installation

First, install the required dependencies, see the list and instructions below.
Then run `make install`. You may do this either as a user or as root.

## Dependencies

Required:
- perl (version 5.14 or later)
- gpg
- JSON (perl module)
- Try::Tiny (perl module)
- Math::Random::Secure (perl module)

Optional:
- xclip (for X11 clipboard support)
- wl-copy (for Wayland clipboard support)
- git (for synchronization support

### Dependency installation

#### Debian and derivatives

To install all dependencies on a Debian-based distribution:

    apt-get install libjson-perl libtry-tiny-perl libmath-random-secure-perl gnupg xclip git gnupg-agent wl-clipboard

#### Arch and derivatives
To install all dependencies on a Arch-based distribution (note that Arch does
not package the Math::Random::Secure module, so you need to use cpanm to
install it):

    pacman -S perl-json perl-try-tiny gnupg xclip git wl-clipboard cpanminus
    sudo cpanm Math::Random::Secure

#### Fedora, Mageia and derivatives

To install all dependencies on a Fedora- or Mageia-based distribution:

    yum install perl-JSON perl-Try-Tiny perl-Math-Random-Secure gnupg xclip git wl-clipboard

## License

gpgpwd is Copyright © Eskild Hustvedt 2012-2022

gpgpwd is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

gpgpwd is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see https://www.gnu.org/licenses/gpl-3.0.txt.
