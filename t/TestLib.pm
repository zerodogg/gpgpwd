package TestLib;
use strict;
use warnings;
use 5.010;
use Exporter qw(import);
use Expect;
use Cwd qw(realpath);
use IPC::Open3 qw(open3);
use File::Basename qw(dirname);
use File::Temp qw(tempdir);
use File::Path qw(remove_tree);
our @EXPORT = qw(t_wait_eof t_expect t_exitvalue eSpawn getCmd expect_send set_gpgpwd_database_path enable_raw_gpgpwd InPath printd gitVersionGreaterThanOrEqualTo);

our $e;
our $testfile;
my $useRawCMD = 0;

if (!defined($ENV{GNUPGHOME}))
{
    $ENV{GNUPGHOME} = $ENV{HOME}.'/.gnupg';
}
delete $ENV{EMAIL};
if ($ENV{GPGPWD_TESTRUNNER_SECOND})
{
    # Under the testrunner we manually clean
    remove_tree($ENV{HOME}.'/.config');
}
else
{
    my $tmpHome = tempdir('gpgpwdt-home-XXXXXXXX',TMPDIR => 1, CLEANUP => 1);
    $ENV{HOME} = $tmpHome;
    open(my $out,'>',$tmpHome.'/.gitconfig');
    print {$out} "[user]\n\tname = gpgpwd tests\n\temail = tests\@example.com";
    close($out);
}

sub printd
{
    if ($ENV{GPGPWD_T_DEBUG})
    {
        print join(' ',@_)."\n";
    }
}

sub t_exitvalue
{
    my $value = shift;
    my $name = shift;
    $e->expect(10,undef);
    if ($value eq 'nonzero')
    {
        main::ok($e->exitstatus != 0, $name);
    }
    else
    {
        main::is($e->exitstatus,$value,$name);
    }
    $e = undef;
}

sub set_gpgpwd_database_path
{
    $testfile = shift;
    printd('Set gpgpwd db path to '.$testfile);
}

sub enable_raw_gpgpwd
{
    $useRawCMD = 1;
}

sub expect_send
{
    return $e->send(@_);
}

sub t_wait_eof
{
    $e->expect(10,undef);
    $e = undef;
}

sub t_expect
{
    my @tests;
    push(@tests,shift);
    if ($tests[0] =~ /^-/)
    {
        push(@tests,shift);
    }
    my $name = shift;
    my $match = $e->expect(4,@tests);
    if ($match)
    {
        main::ok(1,$name);
    }
    else
    {
        main::is($e->before,$tests[-1],$name);
    }
}

sub eSpawn
{
    if(defined $e)
    {
        t_wait_eof();
    }
    my @cmd = getCmd(@_);
    printd('Command: ',@cmd);
    $e = Expect->spawn(getCmd(@_));
    if (!@ARGV || $ARGV[0] ne '-v')
    {
        $e->log_stdout(0);
    }
    return $e;
}

sub getCmd
{
    my @params = @_;
    state $dir = dirname(realpath($0));
    no warnings;
    my $binName = 'gpgpwd';
    if ($ENV{GPGPWD_TEST_BINNAME})
    {
        $binName = $ENV{GPGPWD_TEST_BINNAME};
    }
    if ($ENV{GPGPWD_TEST_VERBOSE})
    {
        push(@params,'-vvvvvvvv');
    }
    if ($useRawCMD)
    {
        return ('perl',$dir.'/../'.$binName,@params);
    }
    return ('perl',$dir.'/../'.$binName,'--password-file',$testfile,@params);
}

sub InPath
{
    foreach (split /:/, $ENV{PATH}) { if (-x "$_/@_" and ! -d "$_/@_" ) {    return "$_/@_"; } } return 0;
}

# Purpose: Retrieve the git version
# Usage: my ($major,$minor,$patch) = gitVersion();
sub gitVersion
{
    state @gitVersion;
    if (scalar(@gitVersion) == 0)
    {
        my $version = getVersionFrom(qw(git --version));
        @gitVersion = split(/\./,$version);
    }
    return @gitVersion;
}

# Purpose: Check if the git version is equal to or above the specified version
# Usage: boolean = gitVersionGreaterThanOrEqualTo(major,minor,patch);
sub gitVersionGreaterThanOrEqualTo
{
    my($maj,$min,$patch) = @_;
    my @gitVersion = gitVersion();
    if ($gitVersion[0] > $maj)
    {
        return 1;
    }
    if ($gitVersion[0] == $maj && $gitVersion[1] > $min)
    {
        return 1;
    }
    if ($gitVersion[0] == $maj && $gitVersion[1] == $min && $gitVersion[2] >= $patch)
    {
        return 1;
    }
    return 0;
}

# Purpose: Get the version of a shell utility
# Usage: version = getVersionFrom('command');
sub getVersionFrom
{
    if (!InPath($_[0]))
    {
        return 'not installed';
    }
    open3(my $in, my $out, my $err,@_);
    my $data;
    if ($out)
    {
        while(<$out>)
        {
            $data .= $_;
        }
    }
    if ($err)
    {
        while(<$err>)
        {
            $data .= $_;
        }
        close($err);
    }
    close($in);close($out);
    if(defined $data)
    {
        $data =~ s/^\D+(\S+).+/$1/s;
        return $data;
    }
    return;
}

1;
